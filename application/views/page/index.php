<!DOCTYPE html>
<html>
    <head>
        <!-- Site made with Mobirise Website Builder v5.8.14, https://mobirise.com -->
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="generator" content="Mobirise v5.8.14, mobirise.com">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
       <!--  <link rel="shortcut icon" href="assets/images/logo.png" type="image/x-icon"> -->
        <meta name="description" content="">
        <title>PT . Shippindo Teknologi Logistik</title>
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
        <link rel="stylesheet" href="assets/dropdown/css/style.css">
        <link rel="stylesheet" href="assets/socicon/css/styles.css">
        <link rel="stylesheet" href="assets/theme/css/style.css">
        <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="preload" href="https://fonts.googleapis.com/css2?family=Jost:ital,wght@0,400;0,700;1,400;1,700&display=swap&display=swap" as="style" onload="this.onload=null;this.rel='stylesheet'">
        <noscript>
            <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Jost:ital,wght@0,400;0,700;1,400;1,700&display=swap&display=swap">
        </noscript>
        <link rel="preload" as="style" href="assets/mobirise/css/mbr-additional.css">
        <link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css">
        <style type="text/css">
            .wizard {
                margin-left: 30px;
                position: relative;

                .v-line {
                    width: 1px;
                    background-color:#baffd1;
                    position: absolute;
                    margin-left: -12px;
                    margin-top: 20px;
                    height: 410px;
                }

                .step1 {
                    padding: 15px 0;
                    position: relative;

                    &:before {
                        content: "";
                        display: block;
                        width: 15px;
                        height: 15px;
                        border-radius: 20px;
                        background-color:#f7fffa;
                        position: absolute;
                        margin-left: -20px;
                        top: 17px;
                    }
                }

                .step2 {
                    padding: 15px 0;
                    position: relative;

                    &:before {
                        content: "";
                        display: block;
                        width: 15px;
                        height: 15px;
                        border-radius: 20px;
                        background-color:#00cc00;
                        position: absolute;
                        margin-left: -20px;
                        top: 17px;
                    }
                }
            }
        </style>
    </head>
    <body>
        <section data-bs-version="5.1" class="menu menu1 cid-tLm3M9y702" once="menu" id="menu1-0">
            <nav class="navbar navbar-dropdown navbar-fixed-top navbar-expand-lg">
                <div class="container-fluid">
                    <div class="navbar-brand">
                        <span class="navbar-caption-wrap">
                            <a class="navbar-caption text-white display-7" href="<?=base_url()?>">PT . Shippindo Teknologi Logistik</a>
                        </span>
                    </div>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-bs-toggle="collapse" data-target="#navbarSupportedContent" data-bs-target="#navbarSupportedContent" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                        <div class="hamburger">
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav nav-dropdown nav-right" data-app-modern-menu="true">
                            <li class="nav-item">
                                <a class="nav-link link text-white display-4" href="<?=base_url()?>">Tentang kami</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link link text-white display-4" onclick="cekresi()">Cek Resi</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link link text-white display-4" href="<?=$link?>">Login</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </section>
        <section data-bs-version="5.1" class="image1 cid-tLm4PBAhNI" id="image1-1">
            <div>
                <div class="row align-items-center">
                    <div class="col-12 col-lg-4">
                        <div class="image-wrapper">
                            <img src="assets/images/screenshot-3.png" alt="">
                            <p class="mbr-description mbr-fonts-style pt-2 align-center display-4"></p>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="text-wrapper">
                            <h3 class="mbr-section-title mbr-fonts-style mb-3 display-5">
                                <strong>Kirim Barang Tanpa Khawatir</strong>
                            </h3>
                            <p class="mbr-text mbr-fonts-style display-7">Apapun barang yang anda kirim jangan khawatir <br>Karena kami akan menjaga barang anda dengan baik <br>dengan layanan cepat sampai aman dan terpercaya. <br>
                                <br>Layanan yang hadir untuk memudahkan para customers <br>dalam mengirimkan barang. Dimana anda bisa melakukan <br>order pengiriman melelui website kami, dan order <br>pengiriman sesuai dengan pemesanan anda.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section data-bs-version="5.1" class="image2 cid-tLm4TiAi7W" id="image2-2">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12 col-lg-4">
                        <div class="image-wrapper">
                            <img src="assets/images/screenshot-4.png" alt="" width="200 px">
                            <p class="mbr-description mbr-fonts-style mt-2 align-center display-4"></p>
                        </div>
                    </div>
                    <div class="col-12 col-lg">
                        <div class="text-wrapper">
                            <h3 class="mbr-section-title mbr-fonts-style mb-3 display-5">
                                <strong>PT Shippindo Teknologi Logistik</strong>
                            </h3>
                            <p class="mbr-text mbr-fonts-style display-7">Merupakan perusahaan yang bergerak di bidang pengiriman <br>barang ekspedisi untuk mengirimkan barang, dari satu <br>tempat ke tempat lain, secara aman dan nyaman. Untuk <br>itu kami PT Shippindo Teknologi Logistik terlahir untuk <br>membantu dalam mengirimkan barang ke luar Kota maupun <br>keluar Pulau yang telah kami tentukan. </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <div class="resi" id="resi">
        <section data-bs-version="5.1" class="content15 cid-tLmeJlQgg8" id="content15-a">
            <hr style="height:30px; width:100%; border-width:0; color:green; background-color:#808080;">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="card col-md-12 col-lg-8">
                        <div class="card-wrapper">
                            <div class="card-box align-left">
                                <h4 class="card-title mbr-fonts-style mbr-white mb-3 display-5" style="text-align: center;">
                                    <strong>Cek Resi</strong>
                                </h4>
                                <!-- Form untuk pencarian resi -->
                                <form action="" method="POST" class="mbr-form form-with-styler mx-auto" data-verified="">
                                    <div class="row">
                                        <div class="dragArea row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 form-group mb-3" data-for="name">
                                                <input type="text" name="resi" data-form-field="resi" class="form-control" value="" id="id_resi">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5"></div>
                                            <div class="col-md-7">
                                                <button type="button" class="btn btn-success display-4" style="text-align: center;" onclick="getResi()">Cek</button>
                                            </div>
                                        </div>
                                </form>
                                <!-- Form untuk pencarian resi -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section data-bs-version="5.1" class="content15" id="content15-a">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="card col-md-12 col-lg-8">
                        <div class="card-wrapper">
                            <div class="card-box align-left">
                                <hr style="height:3px; width:100%; border-width:0; color:green; background-color:#808080;  border-radius:5px;">
                                <h4 class="card-title mbr-fonts-style mb-3 display-7">
                                    <strong>Informasi Pengiriman</strong>
                                </h4>
                                <table>
                                    <tr>
                                        <td>No Resi</td>
                                        <td>:</td>
                                        <td id="noresi"></td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal Pengiman</td>
                                        <td>:</td>
                                        <td id="tanggal"></td>
                                    </tr>
                                    <tr>
                                        <td>Nama Pengirim</td>
                                        <td>:</td>
                                        <td id="pengirim"></td>
                                    </tr>
                                    <tr>
                                        <td>Alamat</td>
                                        <td>:</td>
                                        <td id="alamat"></td>
                                    </tr>
                                    <tr>
                                        <td>Telepon</td>
                                        <td>:</td>
                                        <td id="telepon"></td>
                                    </tr>
                                    <tr>
                                        <td>Isi Barang</td>
                                        <td>:</td>
                                        <td id="isibarang"></td>
                                    </tr>
                                    <tr>
                                        <td>Berat Barang</td>
                                        <td>:</td>
                                        <td id="beratbarang"></td>
                                    </tr>
                                    <tr>
                                        <td>Layanan</td>
                                        <td>:</td>
                                        <td id="layanan"></td>
                                    </tr>
                                    <tr>
                                        <td>Total Harga</td>
                                        <td>:</td>
                                        <td id="total"></td>
                                    </tr>
                                    <tr>
                                        <td>Status</td>
                                        <td>:</td>
                                        <td id="status"></td>
                                    </tr>
                                </table>
                                <hr style="height:3px; width:100%; border-width:0; color:green; background-color:#808080;  border-radius:5px;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
       <!--  <section data-bs-version="5.1" class="content15" id="content15-a">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="card col-md-12 col-lg-8">
                        <div class="card-wrapper">
                            <div class="card-box align-left">
                                <h4 class="card-title mbr-fonts-style mb-3 display-7">
                                    <strong>Status pengiriman</strong>
                                </h4>
                                    <div class="btn-group" style="margin-bottom: 10px;">
                                        <select name="status" id="status" class="form-control">
                                            <option value="1">-- Pilih Status Kiriman -- </option>
                                            <option value="1">Pengambilan</option>
                                            <option value="2">Penerimaan</option>
                                            <option value="3">Sedang Diproses</option>
                                            <option value="4">Dalam Pengiriman</option>
                                            <option value="4">Sudah Diterima</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="card">
                                            <div class="card-header text-white" style="background-color: #1a6626;"><strong>Status terakhir</strong></div>
                                            <div class="card-body bg-dark">
                                                <p class="card-text text-white">Paket Sudah Diterima Oleh Joni</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="card">
                                            <div class="card-header text-white" style="background-color: #fd7e14;"> <strong>Histori Pengiriman</strong></div>
                                            <div class="card-body bg-dark">
                                                <div class="wizard">
                                                    <div class="v-line"></div>
                                                    <div class="step1 text-warning">
                                                        <span><strong>Pengambilan</strong></span>
                                                        <br>
                                                        <span class="text-white"><i>Diambil oleh Nurdian</i></span>
                                                        <br>
                                                        <span class="text-white"><i>28-07-2023 12:00</i></span>
                                                    </div>
                                                    <div class="step1 text-warning">
                                                        <span><strong>Penerimaan</strong></span><br>
                                                        <span class="text-white"><i>Diterima oleh Joko</i></span>
                                                        <br>
                                                        <span class="text-white"><i>28-07-2023 12:00</i></span>
                                                    </div>
                                                    <div class="step1 text-warning">
                                                        <span><strong>Sedang Diproses</strong></span>
                                                        <br>
                                                        <span class="text-white"><i>Dalam Proses Analisa</i></span>
                                                        <br>
                                                        <span class="text-white"><i>28-07-2023 12:00</i></span>
                                                    </div>
                                                    <div class="step1 text-warning">
                                                        <span><strong>Dalam Pengiriman</strong></span><br>
                                                        <span class="text-white"><i>Kurir Abdul Aziz </i></span>
                                                        <br>
                                                        <span class="text-white"><i>28-07-2023 12:00</i></span>
                                                    </div>
                                                    <div class="step2 text-warning">
                                                        <span><strong>Sudah Diterima</strong></span><br>
                                                        <span class="text-white"><i>Diterima Oleh Joni</i></span>
                                                        <br>
                                                        <span class="text-white"><i>28-07-2023 12:00</i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
     </div> -->
        <section data-bs-version="5.1" class="content13 cid-tLm5dTnVQ0" id="content13-4">
            <section data-bs-version="5.1" class="content15 cid-tLmeJlQgg8" id="content15-a">
                <hr style="height:30px; width:100%; border-width:0; color:green; background-color:#808080;">
                <div class="container-fluid">
                    <div class="row justify-content-center">
                        <div class="col-md-12 col-lg-12">
                            <h3 class="mbr-section-title mbr-fonts-style mb-4 display-5">
                                <strong>Kontak Kami</strong>
                            </h3>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-12 col-lg-12">
                            <div class="row justify-content-center">
                                <div class="col-12 col-lg-6">
                                    <span id="boot-icon" class="fa fa-map-marker" style="font-size:1.5rem"></span> Jl. Raya kali Abang Kec. Medan Satria Kota bekasi, 17132 <br>
                                    <span id="boot-icon" class="fa fa-phone" style="font-size:1.5rem"></span> +6280332160215
                                </div>
                                <div class="col-12 col-lg-6">
                                    <ul class="list mbr-fonts-style display-7"></ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <script src="assets/bootstrap/js/bootstrap.bundle.min.js"></script>
            <script src="assets/smoothscroll/smooth-scroll.js"></script>
            <script src="assets/ytplayer/index.js"></script>
            <script src="assets/dropdown/js/navbar-dropdown.js"></script>
            <script src="assets/theme/js/script.js"></script>
    </body>
</html>

<script type="text/javascript">
    function cekresi(){
    var x = document.getElementById("resi");
     if (x.style.display === "none") {
       x.style.display = "block";
      } else {
       x.style.display = "none";
     }
    }
    document.getElementById("resi").style.display ="none";


    function getResi(){
    var id_resi = document.getElementById("id_resi").value;

    console.log('<?=base_url()?>transaksi/get_data');
       $.ajax(
      {
         type: 'post',
         url: '<?=base_url()?>transaksi/get_data',
         data: { 
           "id": id_resi
         },
         success: function (response) {
           alert("Success !!");
           var json = JSON.parse(response);
           console.log(json[0].alamat_penerima);
           console.log(json);
           document.getElementById("noresi").innerHTML =json[0].no_resi;
           document.getElementById("tanggal").innerHTML =json[0].tanggal_pengiriman;
           document.getElementById("pengirim").innerHTML =json[0].nama_pengirim;
           document.getElementById("alamat").innerHTML =json[0].alamat_penerima;
           document.getElementById("telepon").innerHTML =json[0].no_telepon_pengirim;
           document.getElementById("isibarang").innerHTML =json[0].isi_barang;
           document.getElementById("beratbarang").innerHTML =json[0].berat_barang+' Kg';
           document.getElementById("layanan").innerHTML =json[0].layanan;
           document.getElementById("total").innerHTML =json[0].total_harga;
           document.getElementById("status").innerHTML ="<b>"+json[0].status+"</b>";
          },
         error: function () {
           alert("Error !!");
         }
      });
    }


</script>