<?= validation_errors(
	'<div class="alert alert-danger alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>',
	'</div>'
); ?>


<div class="box">
	<div class="box-header">
		<h4>Informasi Pengiriman</h4>
	</div>
	<div class="box-body">
		<form action="<?= base_url('transaksi/add'); ?>" class="form" method="POST">
			
			<div class="form-group">
				<label>No Resi</label>
				<input type="text" class="form-control" name="no_resi" value="<?php echo "0".date('YmdHis') ?>" readonly>
			</div>
			
			<div class="form-group">
				<label>Tanggal</label>
				<input type="date" class="form-control" name="tanggal_pengiriman" value="">
			</div>

			<div class="form-group">
				<label>Nama Pelanggan</label>
				<input type="hidden" class="form-control" name="id_pelanggan" 
				value="<?php echo $this->session->userdata('id_pelanggan')?>" readonly>
				<input type="text" class="form-control" name="namapelanggan" value="<?php echo $this->session->userdata('username')?>" readonly>
			</div>

			<div class="form-group">
				<label>Alamat</label>
				<input type="text" class="form-control" name="alamat_pengirim" value="">
			</div>
			
			<div class="form-group">
				<label>Telepon</label>
				<input type="text" class="form-control" name="telepon" value="<?php echo $this->session->userdata('no_telepon')?>"readonly>
			</div>

		    <div class="box-header">
				<h4>Informasi Pengiriman</h4>
			</div>
			
			<div class="form-group">
				<label>Nama Penerima</label>
				<input type="text" class="form-control" name="namapenerima" value="">
			</div>
			<div class="form-group">
				<label>Alamat</label>
				<input type="text" class="form-control" name="alamat_penerima" value="">
			</div>
			<div class="form-group">
				<label>Telepon</label>
				<input type="text" class="form-control" name="telepon_penerima" value="">
			</div>
			<div class="box-header">
				<h4>Informasi Barang</h4>
			</div>
			
			<div class="form-group">
				<label>Isi Barang</label>
				<input type="text" class="form-control" name="isibarang" value="">
			</div>
			<div class="form-group has-feedback">
				<label>Berat Barang</label>
				<input type="number" class="form-control" name="berat_barang" id="berat_barang" value="0"  onchange="abs()">
				<span class="form-control-feedback">kg</span>
			</div>
			<div class="form-group">
				<label>layanan</label>
			  <select name="layanan" id="layanan" class="form-control" onchange="hitung()">
			  	<option value="">--Pilih Layanan--</option>
  				<option value="1">Yes</option>
 				<option value="2">Reguler</option>
  				<option value="3">Oke</option>
			</select>
			</div>
			<div class="form-group">
				<label>Total Harga</label>
				<input type="text" class="form-control" name="hargatotal" id="hargatotal" value="" readonly>
			</div>
			
			<a href="<?= base_url('menu') ?>" class="btn btn-sm btn-warning" style ="float: right; margin-left: 5px;">Kembali</a>
			<button type="submit" class="btn btn-sm btn-primary" style ="float: right;">Simpan</button>
		
		</form>	
	</div>
</div>

<script type="text/javascript">

 function abs(){
 	var berat   = document.getElementById("berat_barang").value;

 	if(berat < 1){
 		alert("Berat Barang Minial 1 KG");
 		document.getElementById("berat_barang").value =1;
 	}
 	
 }

 function hitung(){
 	var berat   = document.getElementById("berat_barang").value;
 	var layanan =document.getElementById("layanan").value;
 	var hargatotal;

 	if(layanan == 1){
 		hargatotal = berat * 20000;
 	}else if(layanan == 2){
 		hargatotal = berat * 18000;
 	}else if(layanan == 3){
 		hargatotal = berat * 13000;
 	}else{
 		hargatotal =0;
 	}
 	document.getElementById("hargatotal").value =hargatotal;
 	
 }




</script>