<?= $this->session->flashdata('message'); ?>
<div class="box">
    <div class="box-body">
        <div class="table-responsive">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th style="text-align: center; width:5px;">No</th>
                    <th>No Resi</th>
                    <th>Tanggal</th>
                    <th>Nama Pengirim</th>
                    <th>Alamat Pengirim</th>
                    <th>Telepon Pengirim</th>
                    <th>Nama Penerima</th>
                    <th>Alamat Penerima</th>
                    <th>Telepon Penerima</th>
                    <th>Isi Barang</th>
                    <th>Berat Barang</th>
                    <th>Layanan</th>
                    <th>Status</th>
                    <th>Total Harga</th>
                    <?php 
                    if($this->session->userdata('role_id') != 3){
                      echo "<th>Aksi</th>";
                    }?>
                </tr>
            </thead>
            <tbody>
                <?php

                $x = 1;
                foreach ($transaksi as $key=>$m) : ?>
                    <tr>
                        <td style="text-align: center;"><?= $x++; ?></td>
                        <td><?= $m['no_resi']; ?></td>
                        <td><?= $m['tanggal_pengiriman']; ?></td>
                        <td><?= $m['nama_pengirim']; ?></td>
                        <td><?= $m['alamat_pengirim']; ?></td>
                        <td><?= $m['no_telepon_pengirim']; ?></td>
                        <td><?= $m['nama_penerima']; ?></td>
                        <td><?= $m['alamat_penerima']; ?></td>
                        <td><?= $m['no_telepon_penerima']; ?></td>
                        <td><?= $m['isi_barang']; ?></td>
                        <td><?= $m['berat_barang']; ?></td>
                        <td><?= $m['layanan']; ?></td>
                        <td><?= $m['status']; ?></td>
                        <td><?= $m['total_harga']; ?></td>
                        <?php 
                         if($this->session->userdata('role_id') != 3){ ?>
                         <td>
                           <a href="<?= base_url('transaksi/edit/') . $m['no_resi']; ?>" class="btn btn-flat btn-xs  btn-warning"><i class="glyphicon glyphicon-pencil"></i></a>
                            <a onclick="return confirm('Hapus ?' );" href="<?= base_url('transaksi/delete/') . $m['no_resi']; ?>" class="btn btn-flat btn-xs  btn-danger"><i class="glyphicon glyphicon-remove"></i></a>
                         </td>
                         <?php }?>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
       </div>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->