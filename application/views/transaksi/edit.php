<?= validation_errors(
	'<div class="alert alert-danger alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>',
	'</div>'
); 
?>
<div class="box">
	<div class="box-header">
		<h4>Informasi Pengiriman</h4>
	</div>
	<div class="box-body">
		<form action="<?= base_url('transaksi/do_edit'); ?>" class="form" method="POST">
			
			<div class="form-group">
				<label>No Resi</label>
				<input type="text" class="form-control" name="no_resi" value="<?=$transaksi[0]['no_resi'] ?>" readonly>
			</div>`
			
			<div class="form-group">
				<label>Tanggal</label>
				<input type="date" class="form-control" name="tanggal_pengiriman" value="<?=$transaksi[0]['tanggal_pengiriman'] ?>" readonly>
			</div>

			<div class="form-group">
				<label>Nama Pelanggan</label>
				<input type="hidden" class="form-control" name="id_pelanggan" 
				value="<?php echo $this->session->userdata('id_pelanggan')?>" readonly>
				<input type="text" class="form-control" name="namapelanggan" value="<?=$transaksi[0]['nama_pengirim']?>" readonly>
			</div>

			<div class="form-group">
				<label>Alamat</label>
				<input type="text" class="form-control" name="alamat_pengirim" value="<?=$transaksi[0]['alamat_pengirim']?>">
			</div>
			
			<div class="form-group">
				<label>Telepon</label>
				<input type="text" class="form-control" name="telepon" value="<?=$transaksi[0]['no_telepon_pengirim']?>" readonly>
			</div>

		    <div class="box-header">
				<h4>Informasi Pengiriman</h4>
			</div>
			
			<div class="form-group">
				<label>Nama Penerima</label>
				<input type="text" class="form-control" name="namapenerima" value="<?=$transaksi[0]['nama_penerima']?>">
			</div>
			<div class="form-group">
				<label>Alamat</label>
				<input type="text" class="form-control" name="alamat_penerima" value="<?=$transaksi[0]['alamat_penerima']?>">
			</div>
			<div class="form-group">
				<label>Telepon</label>
				<input type="text" class="form-control" value="<?=$transaksi[0]['no_telepon_penerima']?>">
			</div>
			<div class="box-header">
				<h4>Informasi Barang</h4>
			</div>
			
			<div class="form-group">
				<label>Isi Barang</label>
				<input type="text" class="form-control" name="isibarang" value="<?=$transaksi[0]['isi_barang']?>">
			</div>
			<div class="form-group has-feedback">
				<label>Berat Barang</label>
				<input type="number" class="form-control" name="berat_barang" id="berat_barang"  onchange="abs()" value="<?=$transaksi[0]['berat_barang']?>">
				<span class="form-control-feedback">kg</span>
			</div>
			<div class="form-group">
				<label>layanan</label>
			  <select name="layanan" id="layanan" class="form-control" onchange="hitung()">
			  	<option value="">--Pilih Layanan--</option>
			  	<?php if($transaksi[0]['layanan'] =='Yes'){?>
			  	<option value="1" selected>Yes</option>
			  	<option value="2">Reguler</option>
			  	<option value="3">Oke</option>
			  	<?php }else if($transaksi[0]['layanan'] =='Reguler'){ ?>
			  	<option value="2" selected>Reguler</option>
			  	<option value="1">Yes</option>
			  	<option value="3">Oke</option>
			  	<?php }else if($transaksi[0]['layanan'] =='Oke'){?>
			  	<option value="1">Yes</option>
			  	<option value="2">Reguler</option>
				<option value="3" selected>Oke</option>
			  	<?php }?>
			</select>
			</div>
			<div class="form-group">
				<label>Total Harga</label>
				<input type="text" class="form-control" name="hargatotal" id="hargatotal" value="<?=$transaksi[0]['total_harga']?>" readonly>
			</div>

			<div class="box-header">
				<h4>Informasi Kurir</h4>
			</div>
			
			<div class="form-group">
				<label>Nama Kurir</label>

				<select name="layanan" id="id_ks" class="form-control" onchange="settup()">
			  	<option value="">--Pilih Layanan--</option>
			  	<?php 
			  	

			  	foreach ($combobox as $key => $val) {?>
			  	
			  	<option value="<?=$val['id_kurir']?>||<?=$val['nama']?>||<?=$val['id_kendaraan']?>||<?=$val['merk']?>" id="id<?=$val['id_kurir']?>"><?=$val['nama']?></option>
			  	<?php }?>
			</select>

				<input type="hidden" class="form-control" name="namakurir" id="namakurir">
				<input type="hidden" class="form-control" name="idkurir" id="idkurir">
			</div>
			<div class="form-group">
				<label>Nama Kendaraan</label>
				<input type="text" class="form-control" name="namakendaraan" id="namakendaraan">
				<input type="hidden" class="form-control" name="idkendaraan" id="idkendaraan">
			</div>
			
			<a href="<?= base_url('transaksi/cetak/').$transaksi[0]['no_resi'] ?>" class="btn btn-sm btn-warning" style ="float: right; margin-left: 5px;">Cetak</a>
			<button type="submit" class="btn btn-sm btn-primary" style ="float: right;">Simpan</button>
		
		</form>	
	</div>
</div>

<script type="text/javascript">

 function abs(){
 	var berat   = document.getElementById("berat_barang").value;

 	if(berat < 1){
 		alert("Berat Barang Minial 1 KG");
 		document.getElementById("berat_barang").value =1;
 	}
 	
 }

 function hitung(){
 	var berat   = document.getElementById("berat_barang").value;
 	var layanan =document.getElementById("layanan").value;
 	var hargatotal;

 	if(layanan == 1){
 		hargatotal = berat * 20000;
 	}else if(layanan == 2){
 		hargatotal = berat * 18000;
 	}else if(layanan == 3){
 		hargatotal = berat * 13000;
 	}else{
 		hargatotal =0;
 	}
 	document.getElementById("hargatotal").value =hargatotal;
 	
 }

function settup() {
	var e = document.getElementById("id_ks").value;
	console.log(e);

	var myArray = e.split("||");
	console.log(myArray[1]);

	document.getElementById("namakurir").value =myArray[1];
	document.getElementById("idkurir").value =myArray[0];
	document.getElementById("namakendaraan").value =myArray[3];
	document.getElementById("idkendaraan").value =myArray[2];
	
}
 




</script>