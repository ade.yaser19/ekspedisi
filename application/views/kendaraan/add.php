<?= validation_errors(
    '<div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>',
    '</div>'
); ?>

<div class="box">
    <div class="box-header">
        <h3 class="box-title">Tambah Kendaraan</h3>
    </div>
    <div class="box-body">
        <form action="<?= base_url('kendaraan/add'); ?>" class="form" method="post">
            <div class="form-group">
                <label>No Polisi</label>
                <input type="text" name="no_polisi" class="form-control" placeholder="No Polisi">
            </div>
            <div class="form-group">
                <label>Merk</label>
                <input type="text" name="merk" class="form-control" placeholder="Merk">
            </div>
            <div class="form-group">
                <label>Tahun</label>
                <input type="number" name="tahun" class="form-control" placeholder="Tahun">
            </div>
            <div class="form-group">
                <label>Warna</label>
                <input type="text" name="warna" class="form-control" placeholder="Warna">
            </div>
            <div class="form-group">
                <label>Kurir</label>
                <select name="id_user" class="form-control select2">
                  <option value="">- Pilih -</option>
                  <?php foreach ($kurir as $value) { ?>
                  <option value="<?php echo $value->id ?>">
                  <?php echo $value->nama ?>
                  </option>
                  <?php } ?>
                </select>
            </div>
             <div class="form-group">
                <label>No Telepon</label>
                <input type="text" name="notelepon" class="form-control" placeholder="No Telepon">
            </div>
            <a href="<?= base_url('kendaraan') ?>" class="btn btn-sm btn-warning" style="float: right;  margin-left: 5px;">Kembali</a>
            <button class="btn btn-sm btn-primary" type="submit" style="float: right;">Simpan</button>
        </form>
    </div>
</div>