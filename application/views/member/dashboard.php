<div class="box">
    <div class="box-body">
    Selamat Datang, <?= $name; ?>
    </div>
    <!-- /.box-body -->
</div>

<?php 

if($role==3){?>
<div class="box">
    <div class="box-body">
      <p>Layanan PT Shippindo Teknologi Logistik </p>
      <p>Layanan jasa yang dimiliki Oleh PT Shippindo Teknologi Logistik sebagai berikut : </p>
      <p> 1. Tujuan<br>
      Layanan yang memberikan kesempatan untuk menagih biaya kiriman kepada penerima, dangan sistem pembayaran dapat dilakukan secara tunai pada saat penyerahan kiriman
    </p>

      <p> 1. Pack (packing)<br>
      Hal yang paling penting saat akan packing adalah memperhatikan jenis barang yang akan dikirim. Tentunya barang dengan karakteristik yang berbeda akan memerlukan pengemasan yang berbeda pula.<br>
      - Perhatian material barang, apakah termasuk barang pecah belah, cairan yang rawan bocor, makanan yang mudah hancu, atau lain sebagainya.<br>
      - Perhatikan ukuran barang dan carilah yang sesuai dengan ukuran barang yang akan di kemas.
    </p>
    <br>

    <p>Untuk barang yang kecil dan ringan seperi kertas atau kain, kamu cukup melapisi dangan kertas atau karton agar tidak terlipat. Pengemasan bahan kain juga cukup mudah, paket cukup dilapisi dengan bahan anti basah yang rapat. Sebaiknya kamu menyediakan ruang yang cukup untuk memberikan tambahan bantalan-bantalan seperti wrap atau kertas serut untuk meredam benturan. Ini adalah tips packing barang yang penting untuk pengemasan produk makanan, produk elektronik atau produk pecah belah.</p>
</div>
<?php }else if($role==2){?>
<div class="box">
    <div class="box-body">
      <table>
        <tr>
          <td style="width:300px; height:100px;border:1px solid black;">
            <i class="fa fa-file" style="font-size:36px; margin-left:10px;"></i>
            <span style="margin-left: 50px;">Status Pengiriman</span>
            <br>
            <span style="margin-left: 120px;"><?= $pengiriman ?></span>
          </td>
        </tr>
      </table>
    </div>
</div>
<?php }else if($role==1){?>
<div class="box">
    <div class="box-body">
      <table>
        <tr>
          <td style="width:300px; height:100px;border:1px solid black;">
            <i class="fa fa-file" style="font-size:36px; margin-left:10px;"></i>
            <span style="margin-left: 50px;">Data Transaksi</span>
            <br>
            <span style="margin-left: 120px;"><?php echo $this->db->count_all('ttm_order'); ?></span>
          </td>
          <td style="width:100px; height:100px;"></td>
          <td style="width:300px; height:100px;border:1px solid black;">
            <i class='fa fa-users' style='font-size:36px;margin-left:10px;'></i>
            <span style="margin-left: 50px;">Data Kurir</span>
            <br>
            <span style="margin-left: 120px;"><?= $count_kurir; ?></span>
          </td>
          <td style="width:100px; height:100px;"></td>
          <td style="width:300px; height:100px;border:1px solid black;">
            <i class="fa fa-truck" aria-hidden="true" style='font-size:36px;margin-left:10px;'></i>
             <span style="margin-left: 50px;">Data Kendaraan</span>
             <br>
             <span style="margin-left: 120px;"><?php echo $this->db->count_all('mst_kendaraan'); ?></span>
          </td>
        </tr>
      </table>
      <br>
      <table>
        <tr>
          <td style="width:300px; height:100px;border:1px solid black;">
            <i class='fa fa-users' style='font-size:36px;margin-left:10px;'></i>
            <span style="margin-left: 50px;">Data Pelanggan</span>
             <br>
             <span style="margin-left: 120px;"><?= $count_kurir; ?></span>
          </td>
          </td>
          <td style="width:100px; height:100px;"></td>
          <td style="width:300px; height:100px;border:1px solid black;">
            <i class='glyphicon glyphicon-user' style='font-size:36px;margin-left:10px;'></i>
            <span style="margin-left: 70px;">User</span>
             <br>
             <span style="margin-left: 120px;"><?php echo $this->db->count_all('users'); ?></span>
          </td>
          </td>
          <td style="width:100px; height:100px;"></td>
          <td style="width:300px; height:100px;border:1px solid black;">
            <i class="fa fa-file" style="font-size:36px; margin-left:10px;"></i>
             <span style="margin-left: 70px;">Laporan</span>
             <br>
             <span style="margin-left: 130px;"><?php echo $this->db->count_all('ttm_order'); ?></span>
          </td>
        </tr>
      </table>
    </div>
</div>
<?php }?>

    <!-- /.box-body -->
</div>
<!-- /.box -->