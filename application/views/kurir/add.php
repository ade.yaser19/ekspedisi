<?= validation_errors(
    '<div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>',
    '</div>'
); ?>

<div class="box">
    <div class="box-header">
        <h3 class="box-title">Tambah Kurir</h3>
    </div>
    <div class="box-body">
        <form action="<?= base_url('kurir/add'); ?>" class="form" method="post">
            <div class="form-group">
                <label>No KTP</label>
                <input type="text" name="no_ktp" class="form-control" placeholder="No KTP">
            </div>
            <div class="form-group">
                <label>Nama Lengkap</label>
                <input type="text" name="nama" class="form-control" placeholder="Nama Lengkap">
            </div>
            <div class="form-group">
                <label>Alamat</label>
                <input type="text" name="alamat" class="form-control" placeholder="Alamat">
            </div>
            <div class="form-group">
                <label>No Telepon</label>
                <input type="text" name="no_telepon" class="form-control" placeholder="No Telepon">
            </div>
            <div class="form-group">
                <label>Password</label>
                <input type="password" name="password" class="form-control" placeholder="Kata Sandi">
            </div>
            <a href="<?= base_url('kurir') ?>" class="btn btn-sm btn-warning" style="float: right;  margin-left: 5px;">Kembali</a>
            <button class="btn btn-sm btn-primary" type="submit" style="float: right;">Simpan</button>
        </form>
    </div>
</div>