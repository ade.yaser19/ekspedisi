<?= $this->session->flashdata('message'); ?>
<div class="row">
    <div class="conttainer">
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3><?= $count; ?></h3>

                    <p>Kurir</p>
                </div>
                <div class="icon">
                    <i class="fa fa-users"></i>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="box">
    <div class="box-body">
        <a href="<?= base_url('kurir/add'); ?>" class="btn btn-flat btn-primary"><i class="glyphicon glyphicon-plus"></i> Tambah</a>
    </div>
</div>
<div class="box">
    <div class="box-header">
        <h3 class="box-title">Data Kurir</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th style="text-align:center; width:5px;">No</th>
                    <th style="text-align:center;">No KTP</th>
                    <th style="text-align:center;">Nama</th>
                    <th style="text-align:center;">Alamat</th>
                    <th style="text-align:center;">No Telepon</th>
                    <th style="text-align:center;">Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $x = 1;
                foreach ($kurir as $v) : ?>
                    <tr>
                        <td style="text-align:center;"><?= $x++; ?></td>
                        <td><?= $v['no_ktp']; ?></td>
                        <td><?= $v['nama']; ?></td>
                        <td><?= $v['alamat']; ?></td>
                        <td><?= $v['no_telepon']; ?></td>
                        <td style="text-align:center;">
                            <a href="<?= base_url('kurir/edit/') . $v['id']; ?>" class="btn btn-flat btn-xs  btn-warning"><i class="glyphicon glyphicon-pencil"></i></a>
                            <a onclick="return confirm('Hapus ?' );" href="<?= base_url('kurir/delete/') . $v['id']; ?>" class="btn btn-flat btn-xs  btn-danger"><i class="glyphicon glyphicon-remove"></i></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->