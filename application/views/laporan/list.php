<?= $this->session->flashdata('message'); ?>
<div class="box">
    
    <div class="box-body">
        <form method="POST">
            <div class="box-body col-xs-12">
                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-5">
                            <label for="tanggal">Tanggal Awal</label>
                              <input type="date" class="form-control" name="tanggal_awal">
                        </div>

                        <div class="col-sm-5">
                            <label for="tanggal">Tanggal Akhir</label>
                              <input type="date" class="form-control" name="tanggal_akhir">
                        </div>
                    </div>
                 </div> 

                <div class="row" style="margin-top: 10px; float: left;">
                    <div class="form-group">
                        <div class="col-sm-12">
                          <input type="submit" name="submit" value="Tampilkan" class="btn btn-sm btn-flat btn-primary">
                          <!-- <br><br>
                          <a href="<?= base_url('cetak_excel'); ?>" class="btn btn-sm  btn-success">Export Excel</a>
                           <a href="<?= base_url('cetak_pdf'); ?>" class="btn btn-sm btn-flat btn btn-success">Export PDF</a> -->
                        </div>
                    </div>
                </div>

            </div>
        </form>
    </div>

    <div class="box-body">
        <div class="table-responsive">
        <table id="exampleY" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th style="text-align: center; width:5px;">No</th>
                    <th>No Resi</th>
                    <th>Tanggal</th>
                    <th>Nama Pengirim</th>
                    <th>Alamat Pengirim</th>
                    <th>Telepon Pengirim</th>
                    <th>Nama Penerima</th>
                    <th>Alamat Penerima</th>
                    <th>Telepon Penerima</th>
                    <th>Layanan</th>
                    <th>Nama Kurir</th>
                    <th>Kendaraan</th>
                    <th>Status Pengiriman</th>
                </tr>
            </thead>
            <tbody>
                <?php

                $x = 1;
                foreach ($pengiriman as $key=>$m) : ?>
                    <tr>
                        <td style="text-align: center;"><?= $x++; ?></td>
                        <td><?= $m['no_resi']; ?></td>
                        <td><?= $m['tanggal_pengiriman']; ?></td>
                        <td><?= $m['nama_pengirim']; ?></td>
                        <td><?= $m['alamat_pengirim']; ?></td>
                        <td><?= $m['no_telepon_pengirim']; ?></td>
                        <td><?= $m['nama_penerima']; ?></td>
                        <td><?= $m['alamat_penerima']; ?></td>
                        <td><?= $m['no_telepon_penerima']; ?></td>
                        <td><?= $m['layanan']; ?></td>
                        <td><?= $m['nama_kurir']; ?></td>
                        <td><?= $m['no_polisi']; ?></td>
                        <td><?= $m['status']; ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
       </div>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->

