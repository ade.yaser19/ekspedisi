<style type="text/css">
 @import url("//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css");
body { margin: 30px; }
.icon {
  position: relative;
  color: #aaa;
  font-size: 16px;
}

.icon input {
  width: 360px;
  height: 32px;q

  background: #fcfcfc;
  border: 1px solid #aaa;
  border-radius: 5px;
  box-shadow: 0 0 3px #ccc, 0 10px 15px #ebebeb inset;
}

.icon input { text-indent: 32px;}
.icon .fa-user {
  position: absolute;
  top: 10px;
  left: 20px;
}
.icon .fa-lock {
  position: absolute;
  top: 10px;
  left: 20px;
}
</style>


<div class="login-box"  style="margin-top:100px;">
    <!-- /.login-logo -->
    <div class="login-box-body" style="border-radius: 25px;width:400px; height:420px; margin-top:120px">
        <?= validation_errors(
            '<div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>',
            '</div>'
        ); ?>
        <?= $this->session->flashdata('message') ?>
        <form action="<?= base_url('auth'); ?>" method="post" style="margin-top:70px;">
             <div class="login-box-msg"><h3>Silakan Login</h3></div>
            <div class="form-group has-feedback">
                <div class="icon">
                 <span class="fa fa-user"></span>
                 <input type="text" name="username" id="username" class="form-control" placeholder="username" value="<?= set_value('username'); ?>">
                </div>
            </div>
            <div class="form-group has-feedback">
                <div class="icon">
                <span class="fa fa-lock"></span>
                 <input type="password" class="form-control" name="password" placeholder="Password">
                </div>
            </div>
            <div class="row">
                <!-- /.col -->
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-success btn-block btn-flat">Masuk</button>
                </div>
                <!-- /.col -->
            </div>
        </form>
        <center><a href="<?= base_url('auth/register'); ?>" class="text-center">Register</a></center>

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->