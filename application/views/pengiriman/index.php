<?= $this->session->flashdata('message'); ?>
<div class="box">
    <div class="box-body">
        <div class="table-responsive">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th style="text-align: center; width:5px;">No</th>
                    <th>No Resi</th>
                    <th>Tanggal</th>
                    <th>Nama Pengirim</th>
                    <th>Alamat Pengirim</th>
                    <th>Telepon Pengirim</th>
                    <th>Nama Penerima</th>
                    <th>Alamat Penerima</th>
                    <th>Telepon Penerima</th>
                    <th>Layanan</th>
                    <th>Nama Kurir</th>
                    <th>Kendaraan</th>
                    <th>Status Pengiriman</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php

                $x = 1;
                foreach ($pengiriman as $key=>$m) : ?>
                    <tr>
                        <td style="text-align: center;"><?= $x++; ?></td>
                        <td><?= $m['no_resi']; ?></td>
                        <td><?= $m['tanggal_pengiriman']; ?></td>
                        <td><?= $m['nama_pengirim']; ?></td>
                        <td><?= $m['alamat_pengirim']; ?></td>
                        <td><?= $m['no_telepon_pengirim']; ?></td>
                        <td><?= $m['nama_penerima']; ?></td>
                        <td><?= $m['alamat_penerima']; ?></td>
                        <td><?= $m['no_telepon_penerima']; ?></td>
                        <td><?= $m['layanan']; ?></td>
                        <td><?= $m['nama_kurir']; ?></td>
                        <td><?= $m['no_polisi']; ?></td>
                        <td><?= $m['status']; ?></td>
                        <td style="text-align:center;">
                            <a onclick="return confirm('Tekan ok untuk konfirmasi paket sudah diantar' );" href="<?= base_url('pengiriman/konfirmasi/') . $m['no_resi']; ?>" class="btn btn-flat btn-xs  btn-success"><i class="fa fa-check"></i></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
       </div>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->