<div class="box">
    <div class="box-header">
        <h3 class="box-title">Edit Pelanggan</h3>
    </div>
    <div class="box-body">
        <?= validation_errors(); ?>
        <form action="<?= base_url('pelanggan/edit/'); ?>" method="post" class="form">
            <input type="hidden" name="id" value="<?= $id; ?>">
            
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" name="nama" value="<?= $nama; ?>" required>
            </div>
            
            <div class="form-group">
                <label for="email">Email</label>
                <input type="text" class="form-control" name="email" value="<?= $email; ?>" required>
            </div>

            <div class="form-group">
                <label for="no_telepon">No Telepon</label>
                <input type="text" class="form-control" name="no_telepon" value="<?= $no_telepon; ?>" required>
            </div>
            
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" name="password" value="" required>
            </div>

            <a href="<?= base_url('pelanggan') ?>" class="btn btn-sm btn-warning" style="float: right; margin-left: 5px;">Kembali</a>
            <button class="btn btn-sm btn-primary" type="submit" style="float: right;">Ubah</button>
        </form>
    </div>
</div>