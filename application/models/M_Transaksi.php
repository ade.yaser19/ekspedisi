<?php 

defined('BASEPATH') or exit('No direct script access allowed');

class M_Transaksi extends CI_Model {

    public function save_header($data)
    {
        return $this->db->insert('ttm_order', $data);
    }

    public function save_detail($data)
    {
        return $this->db->insert('ttd_order', $data);
    }

    public function get_pengiriman($data ='',$segmen='')
    {
        $this->db->select('to2.no_resi,
                           to2.tanggal_pengiriman,
                           to2.nama_pengirim,
                           to2.alamat_pengirim, 
                           to2.no_telepon_pengirim,
                           to2.nama_penerima,
                           to2.alamat_penerima,
                           to2.no_telepon_penerima,
                           to3.isi_barang,
                           to3.berat_barang,
                           ml.layanan,
                           ms.status,
                           to3.total_harga,to2.id_pelanggan');
        $this->db->from('ttm_order to2');
        $this->db->join('ttd_order to3', 'to3.no_resi = to2.no_resi');
        $this->db->join('mst_layanan ml', 'ml.id = to2.layanan');
        $this->db->join('mst_status ms', 'ms.id = to2.status_pengiriman');
        if($this->session->userdata('role_id') == 3){
          $this->db->where('to2.no_resi =',$data);
          $this->db->or_where('to2.id_pelanggan =',$data);
        }
        if($segmen !=''){
            $this->db->where('to2.no_resi =',$data);
        }
        return $this->db->get()->result_array();
    }

    public function get_transaksi()
    {
        $this->db->select('count(*) as jum_trans');
        $this->db->from('ttm_order to2');
        return $this->db->get()->result_array();
    }

    public function get_kurir()
    {
        $this->db->select('count(*) as jum_trans');
        $this->db->from('users');
        $this->db->where('role_id =',2);
        return $this->db->get()->result_array();
    }

    public function get_kendaraan()
    {
        $this->db->select('count(*) as jum_trans');
        $this->db->from('mst_kendaraan');
        return $this->db->get()->result_array();
    }

    public function get_pelanggan()
    {
        $this->db->select('count(*) as jum_trans');
        $this->db->from('users');
        $this->db->where('id_pelanggan !=',0);
        return $this->db->get()->result_array();
    }

    public function get_user()
    {
        $this->db->select('count(*) as jum_trans');
        $this->db->from('users');
        $this->db->where('id_pelanggan =',0);
        return $this->db->get()->result_array();
    }

    public function get_laporan()
    {
        $this->db->select('count(*) as jum_trans');
        $this->db->from('ttm_order to2');
        $this->db->where('status_pengiriman =',5);
        return $this->db->get()->result_array();
    }

    public function get_data_kurir(){
        $this->db->select('u.id as id_kurir,role_id ,u.nama,mk.id as id_kendaraan,mk.merk');
        $this->db->from('users u');
        $this->db->join('mst_kendaraan mk', 'u.id = mk.id_user');
        $this->db->where('role_id =',2);
        return $this->db->get()->result_array();

    }

    public function update_header($data,$id){
      return $this->db->update('ttm_order', $data, ['no_resi' => $id]);
    }

    public function delete_header($id){
      return $this->db->delete('ttm_order', ['no_resi' => $id]);
    }

    

}

/* End of file Menu_model.php */
/* Location: ./application/models/Menu_model.php */
