<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kendaraan_model extends CI_Model
{
    public function get()
    {
        $this->db->select('mst_kendaraan.*, users.nama as nama_kurir');
        $this->db->from('mst_kendaraan');
        $this->db->join('users', 'users.id = mst_kendaraan.id_user');
        return $this->db->get()->result_array();
    }

    public function detail()
    {
        $id = $this->uri->segment(3);
        return $this->db->get_where('mst_kendaraan', ['id' => $id])->row_array();
    }

    public function update($data)
    {
        $id = $this->input->post('id');
        $this->db->update('mst_kendaraan', $data, ['id' => $id]);
    }

    public function hapus()
    {
        $id = $this->uri->segment(3);
        $this->db->delete('mst_kendaraan', ['id' => $id]);
    }
}
