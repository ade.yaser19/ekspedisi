<?php 
defined('BASEPATH') or ('No direct script access allowed');

class Pengiriman_model extends CI_Model {

	public function get_pengiriman($data)
    {
        $this->db->select('to2.no_resi,
                           to2.tanggal_pengiriman,
                           to2.nama_pengirim,
                           to2.alamat_pengirim, 
                           to2.no_telepon_pengirim,
                           to2.nama_penerima,
                           to2.alamat_penerima,
                           to2.no_telepon_penerima,
                           ml.layanan,
                           users.nama as nama_kurir,
                           mst_kendaraan.no_polisi,
                           ms.status');
        $this->db->from('ttm_order to2');
        $this->db->join('mst_layanan ml', 'ml.id = to2.layanan','LEFT');
        $this->db->join('mst_status ms', 'ms.id = to2.status_pengiriman','LEFT');
        $this->db->join('users', 'users.id = to2.id_kurir','LEFT');
        $this->db->join('mst_kendaraan', 'mst_kendaraan.id_user = to2.id_kurir','LEFT');
        $this->db->where('to2.id_kurir =',$data);
        $this->db->where('to2.status_pengiriman !=',5);
        return $this->db->get()->result_array();
    }

    public function get_pengiriman_admin()
    {
        $this->db->select('to2.no_resi,
                           to2.tanggal_pengiriman,
                           to2.nama_pengirim,
                           to2.alamat_pengirim, 
                           to2.no_telepon_pengirim,
                           to2.nama_penerima,
                           to2.alamat_penerima,
                           to2.no_telepon_penerima,
                           ml.layanan,
                           users.nama as nama_kurir,
                           mst_kendaraan.no_polisi,
                           ms.status');
        $this->db->from('ttm_order to2');
        $this->db->join('mst_layanan ml', 'ml.id = to2.layanan','LEFT');
        $this->db->join('mst_status ms', 'ms.id = to2.status_pengiriman','LEFT');
        $this->db->join('users', 'users.id = to2.id_kurir','LEFT');
        $this->db->join('mst_kendaraan', 'mst_kendaraan.id_user = to2.id_kurir','LEFT');
        $this->db->where('to2.status_pengiriman !=',5);
        return $this->db->get()->result_array();
    }

    public function get_pengiriman_admin_laporan()
    {
        $this->db->select('to2.no_resi,
                           to2.tanggal_pengiriman,
                           to2.nama_pengirim,
                           to2.alamat_pengirim, 
                           to2.no_telepon_pengirim,
                           to2.nama_penerima,
                           to2.alamat_penerima,
                           to2.no_telepon_penerima,
                           ml.layanan,
                           users.nama as nama_kurir,
                           mst_kendaraan.no_polisi,
                           ms.status');
        $this->db->from('ttm_order to2');
        $this->db->join('mst_layanan ml', 'ml.id = to2.layanan','LEFT');
        $this->db->join('mst_status ms', 'ms.id = to2.status_pengiriman','LEFT');
        $this->db->join('users', 'users.id = to2.id_kurir','LEFT');
        $this->db->join('mst_kendaraan', 'mst_kendaraan.id_user = to2.id_kurir','LEFT');
        // $this->db->where('to2.status_pengiriman !=',5);
        return $this->db->get()->result_array();
    }

    public function update($data)
    {
        $no_resi = $this->uri->segment(3);
        $this->db->update('ttm_order', $data, ['no_resi' => $no_resi]);
    }

}

/* End of file Pengiriman_model.php */
/* Location: ./application/models/Pengiriman_model.php */