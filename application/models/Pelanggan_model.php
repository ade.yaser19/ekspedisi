<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pelanggan_model extends CI_Model
{
    public function get()
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('role_id','3');
        return $this->db->get()->result_array();
    }

    public function detail()
    {
        $id = $this->uri->segment(3);
        return $this->db->get_where('users', ['id' => $id])->row_array();
    }

    public function update($data)
    {
        $id = $this->input->post('id');
        $this->db->update('users', $data, ['id' => $id]);
    }

    public function hapus()
    {
        $id = $this->uri->segment(3);
        $this->db->delete('users', ['id' => $id]);
    }

    public function sum()
    {
        return $this->db->query("
            SELECT count(*) as count
            FROM users
            WHERE role_id = '3'
        ")->row_object();
    }
}
