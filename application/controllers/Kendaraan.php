<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Kendaraan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->model('Kendaraan_model', 'kendaraan');
    }

    public function kurir()
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('role_id','2');
        return $this->db->get()->result_object();
    }

    public function index()
    {
        $user = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name = $user['nama'];
        $img  = $user['img'];
        $date_created = $user['date_created'];
        $data = [
            'head'          => 'Kendaraan',
            'name'          => $name,
            'img'           => $img,
            'date_created'  => $date_created
        ];

        $data['kendaraan'] = $this->kendaraan->get();
        $this->load->view('templates/head', $data);
        $this->load->view('templates/nav', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('kendaraan/index', $data);
        $this->load->view('templates/footer');
    }

    public function add()
    {
        $post = $this->input->post();
        if ($post) {
            $data = [
                'id_user'       => $this->input->post('id_user'),
                'no_polisi'     => $this->input->post('no_polisi'),
                'merk'          => $this->input->post('merk'),
                'tahun'         => $this->input->post('tahun'),
                'warna'         => $this->input->post('warna'),
                'notelepon'     => $this->input->post('notelepon')
            ];
            $this->db->insert('mst_kendaraan', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                Data berhasil disimpan
                </div>');
            redirect('kendaraan');
        }   

            $user = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
            $name = $user['nama'];
            $img  = $user['img'];
            $date_created = $user['date_created'];
            $kurir  = $this->kurir();

            $data = [
                'title'         => 'Kendaraan',
                'head'          => 'Kendaraan',
                'name'          => $name,
                'img'           => $img,
                'date_created'  => $date_created,
                'kurir'          => $kurir
            ];
            $this->load->view('templates/head', $data);
            $this->load->view('templates/nav', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('kendaraan/add');
            $this->load->view('templates/footer');
    }

     public function edit()
    {
        $user = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name = $user['nama'];
        $img  = $user['img'];
        $date_created = $user['date_created'];
        $kurir   = $this->kurir();
        $data = [
            'title'         => 'Kendaraan',
            'head'          => 'Kendaraan',
            'name'          => $name,
            'img'           => $img,
            'date_created'  => $date_created,
            'kurir'          => $kurir
        ];
        $get = $this->kendaraan->detail();
        $this->load->view('templates/head', $data);
        $this->load->view('templates/nav', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('kendaraan/edit', $get);
        $this->load->view('templates/footer');
        
        $post = $this->input->post();
        if ($post) {
            $id = $this->input->post('id');
            $data = [
                'no_polisi' => $this->input->post('no_polisi'),
                'merk' => $this->input->post('merk'),
                'tahun' => $this->input->post('tahun'),
                'warna' => $this->input->post('warna'),
                'id_user' => $this->input->post('id_user'),
                'notelepon' => $this->input->post('notelepon')
            ];
            $this->kendaraan->update($data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                Data berhasil diubah
                </div>');
            redirect('kendaraan');
        }
    }

    public function delete()
    {
        $id = $this->uri->segment(3);
        $this->kendaraan->hapus();
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data berhasil dihapus
            </div>');
    }
      
}