<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaksi extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_Transaksi', 'transaksi');
    }

    public function index()
    {
        $user           = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name           = $user['nama'];
        $img            = $user['img'];
        $date_created   = $user['date_created'];
        $data = [
            'head'          => '<i class="fa fa-file"> Daftar Pengiriman</i>',
            'name'          => $name,
            'img'           => $img,
            'role'          => $user['role_id'],
            'date_created'  => $date_created
        ];

        $data['transaksi'] = $this->transaksi->get_pengiriman($this->session->userdata('id_pelanggan'),'');


        $this->load->view('templates/head', $data);
        $this->load->view('templates/nav', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('transaksi/index',$data);
        $this->load->view('templates/footer');
    }

    public function add()
    {   
        $post = $this->input->post();

        if ($post) {

            $data_header = [
                'no_resi' => $post['no_resi'],
                'tanggal_pengiriman' => $post['tanggal_pengiriman'],
                'id_pelanggan' => $post['id_pelanggan'],
                'nama_pengirim' => $post['namapelanggan'],
                'alamat_pengirim' => $post['alamat_pengirim'],
                'no_telepon_pengirim' => $post['telepon'],
                'nama_penerima' => $post['namapenerima'],
                'alamat_penerima' => $post['alamat_penerima'],
                'no_telepon_penerima' => $post['telepon_penerima'],
                'layanan' => $post['layanan'],
                'status_pengiriman' => 1
            ];

            $data_detail = [
                'no_resi' => $post['no_resi'],
                'isi_barang' => $post['isibarang'],
                'berat_barang' => $post['berat_barang'],
                'total_harga' => $post['hargatotal']
            ];

            $rest_H = $this->transaksi->save_header($data_header);
            if($rest_H){
                $rest_D = $this->transaksi->save_detail($data_detail);
            }

            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            Order Pengiriman berhasil ditambah
            </div>');
            redirect('transaksi');
        
        }
    }

    public function edit()
    {
        $user           = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name           = $user['nama'];
        $img            = $user['img'];
        $date_created   = $user['date_created'];
        $data = [
            'head'          => '<i class="fa fa-file"> Daftar Pengiriman</i>',
            'name'          => $name,
            'img'           => $img,
            'role'          => $user['role_id'],
            'date_created'  => $date_created
        ];

        $data['transaksi'] = $this->transaksi->get_pengiriman($this->uri->segment('3'),1);
        $data['combobox'] = $this->transaksi->get_data_kurir();
        $this->load->view('templates/head', $data);
        $this->load->view('templates/nav', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('transaksi/edit', $data);
        $this->load->view('templates/footer');
    }

    public function get_data()
    {
        $post = $this->input->post();
        $restdata = $this->transaksi->get_pengiriman($post['id'],'');
        echo json_encode($restdata);
    }

    public function do_edit()
    {  
        $data = array(
        'id_kurir'  => $_POST['idkurir'],
        'kendaraan' => $_POST['idkendaraan']
       );

        $this->transaksi->update_header($data,$_POST['no_resi']);
        $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        Order Pengiriman berhasil di Assign ke '.$_POST['namakurir'].'
        </div>');
        redirect('transaksi');
    }

    public function delete()
    {  
        $this->transaksi->delete_header($this->uri->segment('3'));
        redirect('transaksi');
    }

    public function cetak()
    {  
       ini_set("memory_limit","256M");
       $restdata= $this->transaksi->get_pengiriman($this->uri->segment('3'),1);
       $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(100, 150), true, 'UTF-8', false);
       
       // set document information
       $pdf->SetCreator(PDF_CREATOR);
       $pdf->SetAuthor('Nicola Asuni');
       $pdf->SetTitle('Resi');
       $pdf->SetSubject('Resi');
       $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
       // set header and footer fonts
       $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
       $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
       
       // set default monospaced font
       $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
       $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
       
       // set some language-dependent strings (optional)
       if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
           require_once(dirname(__FILE__).'/lang/eng.php');
           $pdf->setLanguageArray($l);
       }
       
       // ---------------------------------------------------------
       
       // set font
       $pdf->SetFont('times', '', 10);
       
       // add a page
       $pdf->AddPage('L');
       
       // set color for background
       $pdf->SetFillColor(241, 244, 247 );
       
       $style = array(
           'border' => true,
           'hpadding' => 'auto',
           'vpadding' => 'auto',
           'fgcolor' => array(0,0,0),
           'bgcolor' => false, //array(255,255,255),
           'text' => true,
           'font' => 'helvetica',
           'fontsize' => 8,
           'stretchtext' => 4
       );
       $pdf->write1DBarcode($this->uri->segment('3'), 'C39', '', '', '', 17, 0.22, $style, 'N');
       $pdf->setJPEGQuality(75);
       // set some text for example
       $pdf->Image('assets/jne.png', 12, 40, 25, 25, 'PNG', '', '', true, 200, 1, false, false, 0, false, false, false);

       $estimasi =0;

       if($restdata[0]['layanan'] == 'Yes'){
           $estimasi =date('Y-m-d', strtotime('+1 days', strtotime($restdata[0]['tanggal_pengiriman'])));
       }else if($restdata[0]['layanan'] == 'Reguler'){
           $estimasi =date('Y-m-d', strtotime('+3 days', strtotime($restdata[0]['tanggal_pengiriman'])));
       }else if($restdata[0]['layanan'] == 'Oke'){
           $estimasi =date('Y-m-d', strtotime('+4 days', strtotime($restdata[0]['tanggal_pengiriman'])));
       }  

       $txt ='Pengirim :';
       $txt .= $restdata[0]['nama_pengirim'];
       $txt .='   ';
       $txt .='Penerima : ';
       $txt .=$restdata[0]['nama_penerima'];
       $txt2 ='';
       $txt2 .= '
       ';     
       $txt2 .='Tanggal :';
       $txt2 .=$restdata[0]['tanggal_pengiriman'];
       $txt2 .= '
       ';
       $txt2 .='No Pelanggan : ';
       $txt2 .=$restdata[0]['id_pelanggan'];
       $txt2 .= '
       ';
       $txt2 .='Service :  ';
       $txt2 .=$restdata[0]['layanan'];
       $txt2 .= '
       ';
       $txt2 .='Berat : ';
       $txt2 .=$restdata[0]['berat_barang'].' Kg';
       $txt2 .= '
       ';
       $txt2 .='Jumlah Kiriman : ';
       $txt2 .= 1;
       $txt2 .= '
       ';
       $txt2 .='Biaya kirim : ';
       $txt2 .='Rp.'.number_format($restdata[0]['total_harga']);
       $txt2 .= '
       ';
       $txt2 .='Kota Tujuan : ';
       $txt2 .=$restdata[0]['alamat_penerima'];
       $txt2 .= '
       ';
       $txt2 .='Asuransi : ';
       $txt2 .=' Ya';
       $txt2 .= '
       ';
       $txt2 .='BPIK : ';
       $txt2 .='Di Antar Maks. ';
       $txt2 .= $estimasi;
       $txt2 .= '
       ';
       $txt2 .= '
       ';
       $txt2 .= '
       ';
       $txt2 .= '
       ';
       $txt2 .= '
       ';
       $txt2 .= '
       ';
       $txt2 .= '
       ';
       
       // Multicell test
       $pdf->MultiCell(28.5, 5, '', 1, 'L', 1, 0, '', '', true, 0, false, true,800, 'M', true);
       $pdf->MultiCell(35.6, 5, $txt, 1, 'L', 0, 1, '', '', true, 0, false, true,800, 'M', true);
       $pdf->MultiCell(71, 5,$txt2, 1, 'L', 1, 1, 74, 10, true, 0, false, true,800, 'M', true);
       
       $pdf->lastPage();
       
       // ---------------------------------------------------------
       
       //Close and output PDF document
       $pdf->Output('example_005.pdf', 'I');
       
       //============================================================+
       // END OF FILE
       //============================================================+
    }
}
