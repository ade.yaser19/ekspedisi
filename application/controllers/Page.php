<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Page extends CI_Controller
{
    public function index()
    {
        $data['link'] = base_url().'auth';
        $this->load->view('page/index',$data);
    }
}
