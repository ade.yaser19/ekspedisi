<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Kurir extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->model('Kurir_model', 'kurir');
    }

    public function index()
    {
        $user = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name = $user['nama'];
        $img  = $user['img'];
        $date_created = $user['date_created'];
        $count = $this->kurir->sum();
        $data = [
            'head'          => '',
            'name'          => $name,
            'img'           => $img,
            'date_created'  => $date_created,
            'count'         => $count->count
        ];

        $data['kurir'] = $this->kurir->get();

        $this->load->view('templates/head', $data);
        $this->load->view('templates/nav', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('kurir/index', $data);
        $this->load->view('templates/footer');
    }

    public function add()
    {
        $user = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name = $user['nama'];
        $img  = $user['img'];
        $date_created = $user['date_created'];
        $data = [
            'title'         => 'Kurir',
            'head'          => 'Kurir',
            'name'          => $name,
            'img'           => $img,
            'date_created'  => $date_created,
        ];

        $this->form_validation->set_rules('nama', 'Nama', 'trim|required', [
            'required' => 'Nama anda tidak boleh kosong!'
        ]);

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/head', $data);
            $this->load->view('templates/nav', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('kurir/add');
            $this->load->view('templates/footer');
        } else {
            $pass = '123456';
            $data = [
                'role_id'       => 2,
                'no_ktp'        => $this->input->post('no_ktp'),
                'nama'          => $this->input->post('nama'),
                'username'      => $this->input->post('nama'),
                'email'         => $this->input->post('nama').'@gmail.com',
                'alamat'        => $this->input->post('alamat'),
                'no_telepon'    => $this->input->post('no_telepon'),
                'password'      => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
                'img'           => 'default.png',
                'date_created'  => date('Y-m-d')
            ];
            $this->db->insert('users', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                Data berhasil disimpan
                </div>');
            redirect('kurir');
        }
    }

    public function edit()
    {
        $user = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();

        $this->form_validation->set_rules('nama', 'Nama', 'trim|required', [
            'required' => 'Nama tidak boleh kosong'
        ]);
        
        if ($this->form_validation->run() == false) {
            $name = $user['nama'];
            $img  = $user['img'];
            $date_created = $user['date_created'];
            $data = [
                'title'         => 'Kurir',
                'head'          => 'Kurir',
                'name'          => $name,
                'img'           => $img,
                'date_created'  => $date_created,
            ];
            $get = $this->kurir->detail();
            $this->load->view('templates/head', $data);
            $this->load->view('templates/nav', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('kurir/edit', $get);
            $this->load->view('templates/footer');
        } else {
            $id = $this->input->post('id');
            $data = [
                'no_ktp'        => $this->input->post('no_ktp'),
                'nama'          => $this->input->post('nama'),
                'username'      => $this->input->post('nama'),
                'alamat'        => $this->input->post('alamat'),
                'email'         => $this->input->post('nama').'@gmail.com',
                'no_telepon'    => $this->input->post('no_telepon'),
                'password'      => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
            ];
            $this->kurir->update($data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                Data berhasil diubah
                </div>');
            redirect('kurir');
        }
    }

    public function delete()
    {
        $id = $this->uri->segment(3);
        $this->kurir->hapus();
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data berhasil dihapus
            </div>');
        redirect('kurir');
    }
}
