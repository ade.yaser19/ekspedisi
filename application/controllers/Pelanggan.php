<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Pelanggan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->model('Pelanggan_model', 'pelanggan');
    }

    public function index()
    {
        $user = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name = $user['nama'];
        $img  = $user['img'];
        $date_created = $user['date_created'];
        $count = $this->pelanggan->sum();
        $data = [
            'head'          => '',
            'name'          => $name,
            'img'           => $img,
            'date_created'  => $date_created,
            'count'         => $count->count
        ];

        $data['pelanggan'] = $this->pelanggan->get();

        $this->load->view('templates/head', $data);
        $this->load->view('templates/nav', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('pelanggan/index', $data);
        $this->load->view('templates/footer');
    }

    public function add()
    {
        $user = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name = $user['nama'];
        $img  = $user['img'];
        $date_created = $user['date_created'];
        $data = [
            'title'         => 'Pelanggan',
            'head'          => 'Pelanggan',
            'name'          => $name,
            'img'           => $img,
            'date_created'  => $date_created,
        ];

        $this->form_validation->set_rules('nama', 'Nama', 'trim|required', [
            'required' => 'Nama anda tidak boleh kosong!'
        ]);
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.email]', [
            'required'     => 'Email tidak boleh kosong!',
            'valid_email'  => 'Email anda salah!',
            'is_unique'    => 'Email telah terdaftar'
        ]);
        $this->form_validation->set_rules('password1', 'Password', 'trim|required|min_length[5]', [
            'required'      => 'Kata sandi tidak boleh kosong',
            'min_length'    => 'Kata sandi anda terlalu pendek'
        ]);
        $this->form_validation->set_rules('password2', 'Password', 'trim|matches[password1]', [
            'matches'      =>  'Kata sandi tidak sama!'
        ]);

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/head', $data);
            $this->load->view('templates/nav', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('pelanggan/add');
            $this->load->view('templates/footer');
        } else {
            $data = [
                'role_id'       => 3,
                'nama'          => $this->input->post('nama'),
                'no_telepon'    => $this->input->post('no_telepon'),
                'email'         => $this->input->post('email'),
                'password'      => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
                'img'           => 'default.png',
                'date_created'  => date('Y-m-d')
            ];
            $this->db->insert('users', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                Data berhasil disimpan
                </div>');
            redirect('pelanggan');
        }
    }

    public function edit()
    {
        $user = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();

        $this->form_validation->set_rules('nama', 'Nama', 'trim|required', [
            'required' => 'Nama tidak boleh kosong'
        ]);
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email', [
            'required' => 'Email tidak boleh kosong',
            'valid_email' => 'Email salah'
        ]);
        if ($this->form_validation->run() == false) {
            $name = $user['nama'];
            $img  = $user['img'];
            $date_created = $user['date_created'];
            $data = [
                'title'         => 'Pengguna',
                'head'          => 'Pengguna',
                'name'          => $name,
                'img'           => $img,
                'date_created'  => $date_created,
            ];
            $get = $this->pelanggan->detail();
            $this->load->view('templates/head', $data);
            $this->load->view('templates/nav', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('pelanggan/edit', $get);
            $this->load->view('templates/footer');
        } else {
            $id = $this->input->post('id');
            $data = [
                'nama'          => $this->input->post('nama'),
                'no_telepon'    => $this->input->post('no_telepon'),
                'email'         => $this->input->post('email'),
                'password'      => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
            ];
            $this->pelanggan->update($data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                Data berhasil diubah
                </div>');
            redirect('pelanggan');
        }
    }

    public function delete()
    {
        $id = $this->uri->segment(3);
        $this->pelanggan->hapus();
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data berhasil dihapus
            </div>');
        redirect('pelanggan');
    }
}
