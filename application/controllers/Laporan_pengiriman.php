<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Laporan_pengiriman extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('Pengiriman_model', 'pengiriman');
    }

    public function index()
    {
        $user           = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name           = $user['nama'];
        $img            = $user['img'];
        $date_created   = $user['date_created'];
        $data = [
            'head'          => '<i class="fa fa-file"> Laporan Pengiriman</i>',
            'name'          => $name,
            'img'           => $img,
            'role'          => $user['role_id'],
            'date_created'  => $date_created
        ];

        $data['pengiriman'] = $this->pengiriman->get_pengiriman_admin_laporan();

        $this->load->view('templates/head', $data);
        $this->load->view('templates/nav', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('laporan/list',$data);
        $this->load->view('templates/footer');
    }

}

/* End of file Laporan_pengiriman.php */
/* Location: ./application/controllers/Laporan_pengiriman.php */