<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Pengiriman extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('Pengiriman_model', 'pengiriman');
    }

    public function index()
    {
        $user           = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name           = $user['nama'];
        $img            = $user['img'];
        $date_created   = $user['date_created'];
        $data = [
            'head'          => 'Status Pengiriman - Kurir',
            'name'          => $name,
            'img'           => $img,
            'date_created'  => $date_created
        ];

        if ($this->session->userdata('role_id') == '2') {
            $data['pengiriman'] = $this->pengiriman->get_pengiriman($this->session->userdata('user_id'));
        } else {
            $data['pengiriman'] = $this->pengiriman->get_pengiriman_admin();
        }

        $this->load->view('templates/head', $data);
        $this->load->view('templates/nav', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('pengiriman/index', $data);
        $this->load->view('templates/footer');
    }

    public function konfirmasi()
    {
        $id = $this->uri->segment(3);
        $data = [
            'status_pengiriman' => 5
        ];
        $this->pengiriman->update($data);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data berhasil dikonfirmasi
            </div>');
        redirect('pengiriman');
    }

}

/* End of file Pengiriman.php */
/* Location: ./application/controllers/Pengiriman.php */