<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->model('Kurir_model', 'kurir');
        $this->load->model('Pelanggan_model', 'pelanggan');
        $this->load->model('Pengiriman_model', 'pengiriman');
    }

    public function index()
    {
        $user           = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name           = $user['nama'];
        $img            = $user['img'];
        $date_created   = $user['date_created'];
        $count_kurir    = $this->kurir->sum();
        $count_pelanggan = $this->pelanggan->sum();
        
        if ($this->session->userdata('role_id') == '2') {
            $pengiriman = count($this->pengiriman->get_pengiriman($this->session->userdata('user_id')));
        } else {
            $pengiriman = count($this->pengiriman->get_pengiriman_admin());
        }

        $data = [
            'head'          => 'Dashboard',
            'name'          => $name,
            'img'           => $img,
            'role'          => $user['role_id'],
            'date_created'  => $date_created,
            'count_kurir'  => $count_kurir->count,
            'count_pelanggan'  => $count_pelanggan->count,
            'pengiriman'  => $pengiriman
        ];

        $this->load->view('templates/head', $data);
        $this->load->view('templates/nav', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('member/dashboard');
        $this->load->view('templates/footer');
    }

   public function get_data_dasboard(){
    $data['transaksi'] = $this->transaksi->get_transaksi();
    $data['kurir'] = $this->transaksi->get_kurir();
    $data['kendaraan'] = $this->transaksi->get_kendaraan();
    $data['pelanggan'] = $this->transaksi->get_pelanggan();
    $data['user']      = $this->transaksi->get_user();
    $data['laporan']   = $this->transaksi->get_laporan();

    return $data;

   }
}
