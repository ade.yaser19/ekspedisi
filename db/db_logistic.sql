-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 31, 2023 at 07:47 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_logistic`
--

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `url` varchar(50) NOT NULL,
  `is_active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `title`, `icon`, `url`, `is_active`) VALUES
(1, 'Dashboard', 'fa fa-tachometer', 'dashboard', 1),
(2, 'Pengguna', 'fa fa-users', 'users', 1),
(4, 'Menu', 'fa fa-navicon', 'menu', 1),
(5, 'Pengaturan', 'fa fa-gear', 'pengaturan', 1),
(6, 'Role', 'fa fa-shield', 'role', 1),
(7, 'Role Menu', 'fa fa-address-card', 'access', 1),
(8, 'Daftar Pengiriman', 'fa fa-file', 'transaksi', 1),
(9, 'Data Pelanggan', 'fa fa-file', 'pelanggan', 1),
(11, 'Laporan', 'fa fa-file', 'laporan', 1),
(12, 'Order Pengiriman', 'fa fa-file', 'order_pengiriman', 0);

-- --------------------------------------------------------

--
-- Table structure for table `mst_kenadaraan`
--

CREATE TABLE `mst_kenadaraan` (
  `id` int(11) NOT NULL,
  `id_user` int(2) NOT NULL,
  `no_polisi` varchar(8) NOT NULL,
  `merk` varchar(50) NOT NULL,
  `tahun` varchar(4) NOT NULL,
  `warna` varchar(50) NOT NULL,
  `notelepon` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mst_layanan`
--

CREATE TABLE `mst_layanan` (
  `id` int(11) NOT NULL,
  `layanan` varchar(12) NOT NULL,
  `estimasi_min` int(2) NOT NULL,
  `estimasi_max` int(2) NOT NULL,
  `tarif_layanan` int(11) NOT NULL,
  `tb_barang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_layanan`
--

INSERT INTO `mst_layanan` (`id`, `layanan`, `estimasi_min`, `estimasi_max`, `tarif_layanan`, `tb_barang`) VALUES
(1, 'Yes', 0, 1, 20000, 1),
(2, 'Reguler', 2, 3, 18000, 1),
(3, 'Oke', 3, 4, 13000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `mst_pelanggan`
--

CREATE TABLE `mst_pelanggan` (
  `id_pelanggan` int(11) NOT NULL,
  `nama_pelanggan` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `no_telepon` varchar(16) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mst_status`
--

CREATE TABLE `mst_status` (
  `id` int(11) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_status`
--

INSERT INTO `mst_status` (`id`, `status`) VALUES
(1, 'Pengambilan'),
(2, 'Penerimaan'),
(3, 'Sedang Diproses'),
(4, 'Dalam Pengiriman'),
(5, 'Sudah Diterima');

-- --------------------------------------------------------

--
-- Table structure for table `pengaturan`
--

CREATE TABLE `pengaturan` (
  `id` int(11) NOT NULL,
  `nama_sistem` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengaturan`
--

INSERT INTO `pengaturan` (`id`, `nama_sistem`) VALUES
(1, 'PT SHIPINNDO TEKNOLOGI LOGISTIK');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `role` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `role`) VALUES
(1, 'Admin'),
(2, 'Oprasional'),
(3, 'Pelanggan');

-- --------------------------------------------------------

--
-- Table structure for table `ttd_order`
--

CREATE TABLE `ttd_order` (
  `id` int(11) NOT NULL,
  `no_resi` varchar(16) NOT NULL,
  `isi_barang` varchar(255) NOT NULL,
  `berat_barang` int(11) NOT NULL,
  `total_harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ttd_order`
--

INSERT INTO `ttd_order` (`id`, `no_resi`, `isi_barang`, `berat_barang`, `total_harga`) VALUES
(4, '020230731181157', 'Jagung', 1, 20000),
(5, '020230731183955', 'Buku', 2, 26000),
(6, '020230731184056', 'Cobra', 3, 54000);

-- --------------------------------------------------------

--
-- Table structure for table `ttm_order`
--

CREATE TABLE `ttm_order` (
  `no_resi` varchar(15) NOT NULL,
  `tanggal_pengiriman` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_pelanggan` int(11) NOT NULL,
  `nama_pengirim` varchar(255) NOT NULL,
  `alamat_pengirim` text NOT NULL,
  `no_telepon_pengirim` varchar(16) NOT NULL,
  `nama_penerima` varchar(255) NOT NULL,
  `alamat_penerima` text NOT NULL,
  `no_telepon_penerima` varchar(16) NOT NULL,
  `id_kurir` varchar(255) NOT NULL,
  `kendaraan` varchar(225) NOT NULL,
  `status_pengiriman` int(11) NOT NULL,
  `layanan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ttm_order`
--

INSERT INTO `ttm_order` (`no_resi`, `tanggal_pengiriman`, `id_pelanggan`, `nama_pengirim`, `alamat_pengirim`, `no_telepon_pengirim`, `nama_penerima`, `alamat_penerima`, `no_telepon_penerima`, `id_kurir`, `kendaraan`, `status_pengiriman`, `layanan`) VALUES
('020230731181157', '2023-07-30 17:00:00', 2147483647, 'nopita', 'jl.mawar 1 kalibaru bekasi', '085923132318', 'ADE YASER', 'jl.mawar 1 kalibaru bekasi', '085923132315', '', '', 1, 1),
('020230731183955', '2023-06-30 17:00:00', 2147483647, 'nopita', 'jl.mawar 1 kalibaru bekasi', '085923132318', 'ADE YASER', 'jl.mawar 1 kalibaru bekasi', '085923132315', '', '', 1, 3),
('020230731184056', '2023-07-30 17:00:00', 2147483647, 'nopita', 'jl.mawar 1 kalibaru bekasi', '085923132318', 'ADE YASER', 'jl.mawar 2 kalibaru bekasi', '085923132315', '', '', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `no_telepon` varchar(16) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(255) NOT NULL,
  `img` varchar(128) NOT NULL,
  `date_created` date NOT NULL,
  `id_pelanggan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `username`, `nama`, `no_telepon`, `email`, `password`, `img`, `date_created`, `id_pelanggan`) VALUES
(4, 1, 'admin', 'admin', '', 'admin@admin.com', '$2y$10$wKPD3q3p4zhQrISUcHfRTeSLPQY9VyvQxilWMTxEG9MzzessAFK9O', 'images.png', '2023-07-24', 0),
(8, 2, 'petugas', 'petugas', '', 'petugas@petugas.com', '$2y$10$oAdBYej2nXsqqoI8aEXL/OWlsguZCf0Cuh7tV.OMTXUV/QCAMfdAa', 'default.png', '2023-07-24', 0),
(11, 2, '', 'pelanggan', '', 'petugas@pelanggan.com', '$2y$10$oAdBYej2nXsqqoI8aEXL/OWlsguZCf0Cuh7tV.OMTXUV/QCAMfdAa', 'default.png', '2023-07-24', 0),
(13, 3, '', 'Ade Yaser Arafat', '085923132315', 'ade.yaser19@gmail.com', '$2y$10$QvZim6lsZW4uFQzBxLr.1uQG7Mo9pfC0oPgykQMOigsmC0pEVU6eO', 'default.png', '2023-07-30', 0),
(14, 3, '', 'Ade Yaser Arafat', '085923132315', 'admin1@admin.com', '$2y$10$mm1DbldkRZOQHK/G3GXgn.Jo9Jsvbem.3lIgHdQkagG9UaayMcR.C', 'default.png', '2023-07-31', 0),
(15, 3, 'adey', 'Ade Yaser Arafat', '085923132315', 'admin2@admin.com', '$2y$10$G2LWJgYgM/qe8KGlZ/YZrOtzU1o8PuWrGT2i0PUatnyKVqtwcPY4O', 'default.png', '2023-07-31', 0),
(16, 3, 'suneo', 'Suneo', '085923132318', 'suneo@gmai.com', '$2y$10$wyPz9Qy356gWBEaZB4B/yuuPPry6Y4Ioy7N1mEMXYusiQhq0IYsQm', 'default.png', '2023-07-31', 2147483647),
(17, 3, 'nopita', 'mopita', '085923132318', 'nopita@gmial.com', '$2y$10$/mm37jqTIeqfGyVdYMh0ueXd6Qzpj7piIZun.EO3zbxv4qVSNDFcC', 'default.png', '2023-07-31', 2147483647);

-- --------------------------------------------------------

--
-- Table structure for table `user_access_menu`
--

CREATE TABLE `user_access_menu` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_access_menu`
--

INSERT INTO `user_access_menu` (`id`, `role_id`, `menu_id`) VALUES
(1, 1, 1),
(3, 1, 4),
(4, 3, 1),
(5, 1, 5),
(6, 1, 6),
(7, 1, 7),
(11, 1, 2),
(20, 2, 1),
(21, 1, 13),
(22, 2, 13),
(23, 1, 14),
(24, 2, 14),
(27, 1, 15),
(28, 1, 16),
(29, 1, 17),
(30, 1, 18),
(32, 1, 9),
(33, 1, 8),
(34, 1, 12),
(35, 3, 12),
(37, 3, 8);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_kenadaraan`
--
ALTER TABLE `mst_kenadaraan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_layanan`
--
ALTER TABLE `mst_layanan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_status`
--
ALTER TABLE `mst_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengaturan`
--
ALTER TABLE `pengaturan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ttd_order`
--
ALTER TABLE `ttd_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ttm_order`
--
ALTER TABLE `ttm_order`
  ADD PRIMARY KEY (`no_resi`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_access_menu`
--
ALTER TABLE `user_access_menu`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `mst_kenadaraan`
--
ALTER TABLE `mst_kenadaraan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mst_layanan`
--
ALTER TABLE `mst_layanan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `mst_status`
--
ALTER TABLE `mst_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pengaturan`
--
ALTER TABLE `pengaturan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ttd_order`
--
ALTER TABLE `ttd_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `user_access_menu`
--
ALTER TABLE `user_access_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
